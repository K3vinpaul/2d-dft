"""

2D DFT Code for computational physics project"""
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import axes3d
from laplacian import Kinetic
FIG = plt.figure()
AX = FIG.add_subplot(111, projection='3d')

#Here are defined the initial parameters for the calculations
X_MAX = 5.0  # Box size
Y_MAX = 5.0 #box size
NG = 35  # Number of grid points
NN = 2  # Number of states
X_G = np.linspace(-X_MAX, X_MAX, NG**2)
Y_G = np.linspace(-Y_MAX, Y_MAX, NG**2)
D_X = X_G[1] - X_G[0] # Metric of the space FOR X
D_Y = Y_G[1] - Y_G[0] # Metric of the space FOR Y
R_G = []
for i in range(NG**2):
    R_G.append(np.sqrt((X_G[i])**2+(Y_G[i])**2))
D_R = abs(R_G[1] - R_G[0])

#Matriz Cinetic
T = Kinetic(NG)
T_GG = T.kin()

VEXT_G = np.zeros((NG**2, NG**2))
for i in range(0, NG**2):
    VEXT_G[i][i] = 0.5 * (R_G[i])**2 # External potential

# Initialize density as even:
N_G = 2.0 * NN / ((NG)**2 * D_R) * np.ones(NG**2)
print('Initial charge', N_G.sum() * D_R)

# Nn states, each one doubly occupied.
# Initialize as constant density:
VHARTREE_G = np.zeros((NG**2, NG**2))
VX_G = np.zeros((NG**2, NG**2))

#One method to use is the Poisson Method
def poisson_solve(n_g):
    """

    Function to calculate the soft poisson method into the Hartree potential
    """
    vhartree_g = np.zeros(NG**2)
    for k in range(NG**2):
        for j in range(NG**2):
            vhartree_g[k] += n_g[j] / np.sqrt(1+abs(R_G[k] - R_G[j]))**2
    vhartree_g *= D_R
    return vhartree_g

#Here starts the Iteration fixing a tolerance
DENSITY_CHANGE_INTEGRAL = 1.0
while DENSITY_CHANGE_INTEGRAL > 1e-6:
    # Calculate Hamiltonian
    VEFF_G = VEXT_G + VHARTREE_G + VX_G
    H_GG = T_GG + np.diag(VEFF_G)  # Hamiltonian

    # Solve KS equations
    EPS_N, PSI_GN = np.linalg.eigh(H_GG)
    print('Energies', ' '.join('{:4f}'.format(EPS) for EPS in EPS_N[:NN]))

    # Normalize states (states are normalized, but not in our DR metric)
    PSI_GN /= np.sqrt(np.abs(D_R))

    # Update density
    NOLD_G = N_G
    N_G = 2.0 * (PSI_GN[:, :NN]**2).sum(axis=1)
    DENSITY_CHANGE_INTEGRAL = abs(NOLD_G - N_G).sum() * D_R

    CHARGE = np.abs(N_G.sum() * D_R)
    print('Number of electrons', CHARGE)
    print('Convergence err', DENSITY_CHANGE_INTEGRAL)
    assert abs(CHARGE - 2.0 * NN) < 1e-14

    # Calculate Hartree potential
    VHARTREE_G = poisson_solve(N_G)
    EHARTREE = 0.5 * (VHARTREE_G * N_G).sum() * D_R
    print('Electrostatic energy', EHARTREE)
    # Calculate exchange potential (we won't bother with correlation!)
    VX_G = -(3. / np.pi * N_G)**(2. / 3.)
    EX = -3. / 4. * (3. / np.pi)**(1. / 3.) * (N_G**(4. / 3.)).sum() * D_R
    print('Exchange energy', EX)

    EKS = 2.0 * EPS_N[:NN].sum()  # Band structure energy
    EKIN = EKS - (VEFF_G * N_G).sum() * D_R
    print('Ekin', EKIN)
    EPOT = EHARTREE + EX + (VEXT_G * N_G).sum() * D_R
    print('Epot', EPOT)
    ETOT = EKIN + EPOT
    print('Energy', ETOT)

# When the iteration is finished the results are plotted
for i in range(NN):
    #plt.plot(Y_G, PSI_GN[:, i])
    plt.plot(X_G, Y_G, PSI_GN[:, i], label='n={}, e={:3f}'.format(i + 1, EPS_N[i]))
    plt.legend(loc='lower right')
plt.show()
