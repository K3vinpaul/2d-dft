import numpy as np
from scipy import sparse
class Kinetic():
    def __init__(self,grid):
        self.gridz = grid
        self.nx = self.gridz
        self.ny = self.nx
        self.ene = (self.nx)*(self.ny)
        	
    def Laplacian(self):
        main_diag = np.ones(self.ene)*-4.0
        side_diag = np.ones((self.ene)-1)
        side_diag[np.arange(1,self.ene)%4==0] = 0
        up_down_diag = np.ones((self.ene)-self.gridz)
        diagonals = [main_diag,side_diag,side_diag,up_down_diag,up_down_diag]
        laplacian = sparse.diags(diagonals, [0, -1, 1,self.nx,-self.nx], format="csr")
        return laplacian.toarray()




