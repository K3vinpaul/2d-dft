#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for the laplacian interface giving a special form matrix to calculate the laplacian
Created on Thu Apr 25 19:41:29 2019

@author: kevin
"""

from laplacian import Kinetic
T = Kinetic(10)
L = T.laplacian()
K = T.kin()
print(L,K)
