from setuptools import setup

setup(
    name='2d-dft',
    version='1.0',
    packages=[''],
    url='https://gitlab.com/K3vinpaul/2d-dft',
    license='',
    author='Kevin Espinosa de los Monteros',
    author_email='kevin.espinosa@yachaytech.edu.ec',
    description='2D dft code for computatinal physics project'
)
