"""
Class for calculate the laplacian of a 2D vector,
Computational Physics project
Kevin Espinosa de los Monteros"""
import numpy as np
from scipy import sparse
class Kinetic():
    """Interface for the calculation of the laplacian"""
    def __init__(self, grid):
        self.gridz = grid
        self.n_x = self.gridz
        self.n_y = self.n_x
        self.ene = (self.n_x)*(self.n_y)
    def laplacian(self):
        """ Here we construct the matrix using sparce"""
        main_diag = np.ones(self.ene)*- 4.0
        side_diag = np.ones((self.ene)- 1)
        side_diag[np.arange(1, self.ene)%4 == 0] = 0
        up_down_diag = np.ones((self.ene) - self.gridz)
        diagonals = [main_diag, side_diag, side_diag, up_down_diag, up_down_diag]
        laplacian = sparse.diags(diagonals, [0, -1, 1, self.n_x, -self.n_x], format="csr")
        return laplacian.toarray()
    def kin(self):
        """Multipling the result for the laplacina by 1/2 to take the kinetic part"""
        kin = self.laplacian()*-0.5
        return kin
